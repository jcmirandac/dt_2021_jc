# 2: Resource management and sustainable production
## 2.6 Eco-design
---
<i class="fa fa-info-circle" style="color:#4d45c2"><!-- icon --></i>

Essential idea

>Eco-design considers the design of a product throughout its lifecycle (from cradle to grave) using lifecycle analysis.

Nature of design:

>Consideration of the environmental impact of any product, service or system during its life cycle should be instigated at the earliest stage of design and continue through to disposal. Designers should have a ﬁrm understanding of their responsibility to reduce the ecological impact on the planet. Eco design concepts currently have a great inﬂuence on many aspects of design.

Aim:

>The smart phone is an innovative example of converging technologies that combines two technologies in the kitchen into one space saving device. The resultant reduction of materials, and energy used in production and distribution has environmental beneﬁts.

---

From the book (P. Metcalfe, R. Metcalfe, Design & Technology):

>Eco-design, also known as ‘Design for the Environment’, (DfE) represents a movement within the design community to adopt materials and processes that minimise the effects of production and construction on the environment. In 2005 the European Union issued an Eco-Design directive (2005/32/EEC) that obliges manufacturers to reduce energy consumption and other environmental impacts that might otherwise occur throughout the product life-cycle. An Eco-design label was to be applied to products meeting this criterion.

![Eco-design label](images/2_6_eco_design_logo.png)

Eco-design label

##The *‘cradle to grave’* and *‘cradle to cradle’* philosophy

The “cradle-to-grave” concept includes stages of the life cycle of a commercial product, process, or service. It includes raw material extraction and processing (cradle), through the product's manufacture, distribution and use, to the recycling or final disposal of the materials composing it (grave).

The “cradle-to-cradle” is an extension of “cradle to grave’” concept to include recycling at the end of product life. It then goes back to the beginning of the cycle.

##Life cycle analysis (lcA)

An LCA study involves a detailed inventory of the energy and materials that are required across the industry value chain of the product, process or service, and calculates the corresponding emissions to the environment. This information helps designers and manufacturers make decisions based on the impact on the environment. It also has a marketing value, and may be considered for certifications or legislation related matters. Some countries value the development  of **life cycle thinking** strategies as a key concept.

##Product life cycle stages: the role of the designer, manufacturer and user

From the book (P. Metcalfe, R. Metcalfe, Design & Technology):

>**Designers** are charged with the responsibility of bringing products all the way from the conceptual stage through to the consumer. They must precisely predict, target and capture market segments through the continued development of innovative designs. These designs all start with a clearly deﬁned brief. The role of a product designer is to ﬁnd the solution of best ﬁt and encompasses many of the skills and characteristics associated with marketing, manufacturing, materials engineering, aesthetics and ergonomics, generating designs that use fewer materials, produce less waste and contain more recycled and recyclable materials.

>**Manufacturers** have an important role to play in the selection of production processes. They have responsibilities for both quality control and the speed at which products are produced. They must specify standards for components and outsourced sub-assemblies. They are also responsible for product warranties and may even provide aﬅer sales service or technical training in repair procedures. Manufacturers have a role to play in minimizing energy requirements, emissions, water contamination etc. through the adoption of waste management programs designed to recycle or reuse waste products.

>**Users** provide the demand for products. They have the right to purchase products that will perform as advertised. Their patterns of spending, determine whether a more durable product is purchased over a disposable device. Responsible use of products can also reduce power requirements through such facilities as the use of low power or sleep modes. Thoughtful disposal of products and packaging can increase recycling and reduce landﬁll.






<iframe width="560" height="315" src="https://www.youtube.com/embed/7gTdyh8ejQw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/6RNnzfUHwY8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/iD-m6qBij8Q" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/UIrvWVcb4E8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

<i class="fa fa-info-circle" style="color:#4d45c2"><!-- icon --></i>

International-mindedness: 
- The diﬀering stages of economic development of diﬀerent countries/regions and their past and future contributions to global emissions is an issue.

Theory of knowledge:
- There is no waste in nature. Should areas of knowledge look at natural processes beyond human endeavour?

---
