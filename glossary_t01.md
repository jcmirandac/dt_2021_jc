# Glossary for topic 1

From:

Diploma Programme Design technology 
Glossary of terms 
By:

© International Baccalaureate Organization 2015 

Topic 1 : Human factors and ergonomics 

| Term | Definition |
| -- | -- |
| **Adjustability** | The ability of a product to be changed in size, commonly used to increase the range of percentiles that a product is appropriate for. |
| **Anthropometrics** | The aspect of ergonomics that deals with body measurements, particularly those of size, strength and physical capacity. |
| **Biomechanics** | The research and analysis of the mechanics of living organisms. Biomechanics in Human factors includes the research and analysis of the mechanics (operation of our muscles, joints, tendons, etc.) of our human body. It also includes force (impact on user’s joints), repetition, duration and posture. |
| **Clearance** | The physical space between two objects. |
| Cognitive ergonomics | How mental processes, (memory, reasoning, motor response and perception), affect the interactions between users and other components of a system. |
| **Comfort** | A person's sense of physical or psychological ease. |
| **Dynamic data** | Human body measurements taken when the subject is in motion related to range and reach of various body movements. E.g. crawling height, overhead reach and the range of upper body movements. |
| **Environmental factors** | A set of psychological factors that can affect the performance of an individual that come from the environment that the individual is situated. |
| **Ergonomics** | The application of scientific information concerning the relationship between human beings and the design of products, systems and environments. |
| **Fatigue** | A person's sense of physical or psychological tiredness. |
| **Functional data** | Functional data includes dynamic data measurements while performing a required task e.g. reaching abilities, manoeuvring and aspects of space and equipment use. |
| **Human error** | Mistakes made by users, some of which can result in catastrophic consequences for people, property and the environment, as they are considered key contributors to major accidents.  |
| **Human factors** | A scientific discipline concerned with understanding how humans interact with elements of a system. It can also be considered the practice of designing products, systems or processes to take account of the interaction between them and their users. It is also known as comfort design, functional design and user-friendly systems.  |
| **Human information processing system** | An automatic system that a person uses to interpret information and react. It is normally comprised of inputs, processes (which can be sensory, central and motor), and outputs. Interval data Interval data are based on numeric scales in which we know the order and the exact difference between the values. Organised into even divisions or intervals, and intervals are of equal size. |
| **Nominal data scale** | Nominal means 'by name' and used in classification or division of objects into discrete groups. Each of which is identified with a name e.g. category of cars, and the scale does not provide any measurement within or between categories. |
| **Ordinal data** | A statistical data type that exists on an arbitrary numerical scale where the exact numerical value has no significance other than to rank a set of data points. Deals with the order or position of items such as words, letters, symbols or numbers arranged in a hierarchical order. Quantitative assessment cannot be made. |
| **Percentile range** | That proportion of a population with a dimension at or less than a given value. For a given demographic (gender, race, age), the 50th percentile is the average. |
| **Perception** | The way in which something is regarded, understood or interpreted. |
| **Physiological factor data** | Human factor data related to physical characteristics used to optimise the user's safety, health, comfort and performance Primary data Data collected by a user for a specific purpose. |
| **Psychological factor data** | Human factor data related to psychological interpretations caused by light, smell, sound, taste, temperature and texture. |
| **Qualitative data** | Typically descriptive data used to find out in depth the way people think or feel - their perception. Useful for research at the individual or small (focus) group level. |
| **Quantitative data** | Data that can be measured and recorded using numbers. Examples include height, shoe size, and fingernail length. Range of sizes A selection of sizes a product is made in that caters for the majority of a market. |
| **Ratio data scale** | A ratio scale allows you to compare differences between numbers. For example, use a rating scale of 1-10 to evaluate user responses. |
| **Reach** | A range that a person can stretch to touch or grasp an object from a specified position. |
| **Secondary data** | Data collected by someone other than the user. |
| **Static data** | Human body measurements when the subject is still. |
| **Structural data** | Refers to measurements taken while the subject is in a fixed or standard position, e.g. height, arm length. |
| **Workplace environmental factors** | These factors can be considered to maximise performance of a user in a role and reduce the risk of accidents. They can be categorised as: <br/> -Management (policies, safety education)<br/>-Physical environment (noise, temperature, pollutants, trip hazards, signage)<br/>-Equipment design (controls, visibility, hazards, warnings, safety guards)<br/>-The nature of the job (repetitiveness, mental or physical workload, force, pressure)<br/>-Social or psychological environment (Social group, morale)<br/>-The worker (personal ability, alertness, age, fatigue)  |
| -- | -- |


