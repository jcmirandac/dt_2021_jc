# Physiogical factors

---
<i class="fa fa-info-circle" style="color:#4d45c2"><!-- icon --></i>

Essential idea: 

Nature of design:

Aims:

---

Physiological factors encompass the physical aspect of the body. Designers  use a range of physiological data to  inform their design decisions. Some physiological factors that they may consider are:

* Muscle strength in different positions: How strong a muscle is in different positions. 
* Endurance in different positions (how long a position can be maintained before discomfort sets in)
* Visual acuity (how well the user can see under different conditions)
* Tolerance to hot or cold temperatures
* Range of frequencies that can be heard by humans
* Hand/eye coordination

---

### Comfort and fatigue

When people use a product they can put strain and stress on their body. Sitting for long periods of time, or being required to turn a handle put stresses on the body. Designers need to collect data to inform their design decisions. 

* Comfort (being free of physical pain): comfort is an important consideration for designers simply because it influences the way users interact with products. 

* Fatigue (a feeling of tiredness or weakness; happens over time): when people are put under physical or mental stress/activities for extended periods, fatigue can set in. People react differently when they are fatigued. Errors can creep in which then could prove dangerous. Designers need to understand peoples tolerances and design products and environments that help to reduce fatigue.

---

### Biomechanics

Biomechanics is the study the mechanical movements of our body. It focuses on how our body moves and how it is affected by different forces. 

For designers, understanding the range and ability of the human body can help us design products that can comfortably, safely, and efficiently meet the needs of users.

<iframe width="560" height="315" src="https://www.youtube.com/embed/79yH4fCXv88" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/OhN1d0xxUjE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe title="vimeo-player" src="https://player.vimeo.com/video/203132786?h=760c481427" width="640" height="496" frameborder="0" allowfullscreen></iframe>

---
