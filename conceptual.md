# 3: Modelling
## 3.1 Conceptual modelling
---
<i class="fa fa-info-circle" style="color:#4d45c2"><!-- icon --></i>

Essential idea

>A conceptual model originates in the mind and its primary purpose is to outline the principles and basic functions of a design or system.

Nature of design:

>Designers use conceptual modelling to assist their understanding by simulating the subject matter they represent. Designers should consider systems, services and products in relation to what they should do, how they should behave, what they look like and whether they will be understandable by the users in the manner intended.

Aim:

>The starting point for solving a problem springs from an idea developed in the mind. A detailed exploration of the idea is vital to it from the intangible to the tangible, along with the ability to articulate the idea to others.

---

From the book (P. Metcalfe, R. Metcalfe, Design & Technology):

>Conceptual modelling is deﬁned by Preece et al, (2002) as, ‘a description of the proposed system in terms of a set of integrated ideas and concepts about what it should do, behave and look like, that will be understandable by the users in the manner intended.’

A conceptual model is based on the descriptive information of a potential design. This information is related to features, function, shapes, aesthetics, materials, dimensions, etc.

A conceptual model must also be as simple as possible, without leaving out any key feature that may render it ineffective or useless. It must also have clear objectives that will lead the process to a succesful prototype and/or product.

Conceptual modelling is an iterative process and thus the model evolves and improves over time. But, because it is the first step of the process, it will affect all the following stages, and therefore has an intrinsic importance for the project.

The most basic rol of the conceptual model is to ensure that the end result will meet designer's (and user's) expectations.

Conceptual models are:

- a model of concepts or ideas (abstract) that exist in the mind.
- used to help us know and understand, design thinking, ideas, casual relationships, principles, data, systems, algorithms or processes.
- used to illustrate relationships that is in the designers mind to others.
- able to help explain the thinking behind  new ideas.
- able to help us to communicate with other members of  design team, manufacturer or client.
- able to help us visualise ideas through graphic, physical and virtual models.

---
