# 2: Resource management and sustainable production
## 2.2 Waste mitigation strategies
---
<i class="fa fa-info-circle" style="color:#4d45c2"><!-- icon --></i>

Essential idea

>Waste mitigation strategies can reduce or eliminate the volume of material disposed to landfill.

Nature of design:

>The abundance of resources and raw materials in the industrial age led to the development of a throwaway society, and as resources run out, the many facets of sustainability become a more important focus for designers. The result of the throwaway society is large amounts of materials found in landfill, which can be considered as a new source to mine resources from. (2.7)

Aim 2:

>The exploration of possible solutions to eliminate waste in our society has given rise to ideas developed as part of the circular economy. By redesigning products and processes, the waste from one product can become the raw material of another.

---

The abundance of resources and raw materials in the industrial age led to the development of a throwaway society, and as resources run out, the many facets of sustainability become a more important focus for designers. The result of the throwaway society is large amounts of materials found in landfill, which can be considered as a new source to mine resources from.

The exploration of possible solutions to eliminate waste in our society has given rise to ideas developed as part of the circular economy. By redesigning products and processes, the waste from one product can become the raw material of another.

<iframe width="560" height="315" src="https://www.youtube.com/embed/KMu964d_dXY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

### Strategies to Mitigate Waste

Several strategies can be implemented to reduce or eliminate waste throughout the lifecycle of a product. As a designer, you can apply these strategies directly or indirectly to your design. Regardless of the strategies usesd, it is essential to design out as much waste as possible.

#### Re-use

Term: This is reusing a product in the same context or in a different context.

- Reusing is utilising an object more than one time.
- This takes into account of conventional reuse where the object is used again for similar purpose, and new-life reuse where it is used for an innovative purpose.
- An example of reusing is using disposable plastic or glass bottles to drink water from over again.
- Reusing car tyres

#### Recycle

Recycling refers to the use of waste to create a new product. Commonly recycled materials include paper, cardboard, thermoplastics, and alunimum.

#### Repair

Repair refers to fixing or renewing a worn out or broken component. Common examples are bicycle tire repair kits for fixing punctured tubes, or the replacement of cracked cell-phone screens. Designers can make decisions that either empower users to do their won repairs, or decisions that limit or prevent users from making their own repairs. 

It is not uncommon for manufacturers to "lock-out" users from repairing products they own. Recent examples include John Deer farming machines. Movements such as "Right to Repair" advocate for users being able to repair their own products. 

#### Recondition / Refurbish

Reconditioning refers to the rebuilding of the product so that it is in an "as-new condition". Common examples are car engines, mobile phones, and computers. Manufactures usually sell the product at a lower price than the new one, but provide a limited warranty for the reconditioned product.

#### Re-engineer

Re-engineering refers to the redesign of materials or components to improve the performance of a product. It can also be called "Upgrading". This could include upgrading the processor of a computer or changing of gears or components on a bicycle frame. Re-engineering can also involve changing materials to be more environmentally friendly; developing a more efficient manufacturing process which results in some changes to the final product; or introduce a new function to the original design.

---

### Methodologies for Waste Reduction and Designing out Waste

As natural resources become more scare due to consumption, strategies for waste reduction and designing out waste will only become more important for designers. It is becoming essential for designers to consider the impact of their designs and to include waste mitigation strategies as an essential component of the design process.

LCA (Life-Cycle-Analysis) is one such tool that designers can use to measure the impact of their designs. Other strategies include:

- Circular Economy
- Energy Recovery
- WEEE Recovery
- Raw material recovery
- Recycling
- Dematerialization

#### Dematerialization

The reduction of total material and energy throughput of any product and service.

- Dematerialization improves product efficiency by saving, reusing or recycling materials, components and products.
- It impacts on every stage of the product life cycle: in material extraction; eco-design; cleaner production; environmentally conscious consumption patterns; recycling of waste.
- It may mean smaller, lighter products and packaging; the replacement of physical products by virtual products (email instead of paper, web pages instead of brochures); home working, and so on.
- Reduction of total material and energy throughput of a product or service, and the limitation of its environmental impact through: reduction of raw materials at the production stage; energy and material inputs at the user stage; waste at the disposal stage.
- There are potential results of successful dematerialization.

#### Product Recovery Strategies at End of Life/Disposal

The processes of separating the component parts of a product to recover the parts and materials.

     Use and recovery of standard parts at the end of product life.
     Recovery of raw materials.
    Take back legislation.
    Trade in.
    Recycling bins/locations.
    Employ a circular economy.

#### Circular Economy

An economy model in which resources remain in use for as long as possible, from which maximum value is extracted while in use, and the products and materials are recovered and regenerated at the end of the product life cycle. Waste is viewed as a resources and is brought back into the system to generate new products and services.

- In an economic model that is a closed loop system where the materials/resources are in constant use. At the end of the product life cycle the material waste (or obsolete product material) is recycled/recovered.
- The material waste is a resource in the system and is regenerated at he end of the product life cycle.

<iframe width="560" height="315" src="https://www.youtube.com/embed/6OrSOWXkPGM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Within a circular economy,  materials can be classified into two categories

- Biological materials: materials that are renewable and come from nature (organic)
- Technical materials: materials that are manufactured and are finite in their quantities.

The linear economy can be described as:

- Take resources from nature, and generate waste in the process
- Make a produce a product using the materials, and generate waste in the process
- Dispose of the product and thus create waste

On the other hand, a circular economy separates the two types of resources and ensures that they are reused in some capacity. Waste thus becomes a resource for the next iteration. In this way, a circular economy seeks to mimic a healthy biological ecosystem. Biomimicry approaches to design actually include a category that recognizes this distinction.

---

#### Recycling

The system most people are familiar with, recycling is defined as the use of materials from obsolete products to create other products. recycling decreases the demand for new raw materials, reduces energy consumption, reduces waste production, and lowers greenhouse gas emissions.

Different materials have different degrees of recyclability. When using recycled materials, designers need to consider the physical and aesthetic properties of the recycled materials, as well as cost. Aluminum for instance, doesn't degrade to the same degree as most plastics, and as such can be recycled quite easily and frequently and still maintain its desirable physical and aesthetic properties. Most plastics, on the other hand, degrade during the recycling process, and their physical properties change. They can become less transparent, for example. 

---

#### Dematerialization

Dematerialization is the strategy of "doing more with less." At its essence, dematerialization seeks to reduce the energy and materials used in the production, use, and end-of-life of a product, and thus reduce the impact on the environment.

Common examples include:

- shifting from paper to digital communication (letter to emails)
- fossil fuels to solar power
- shrinking or miniaturizing a product so it uses less material

Lightweighting (also known as de-weighting) is the reduction of the quantity of materials to reduce overall weight which results in less material and energy use.  Designers may make use of FEA (Finite Element Analysis) and Generative Design to identify materials, forms, or processes that can reduce weight but still meet the same performance goals. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/20vShlZqUdQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

#### WEEE Recovery

Electronic devices contain a mixture of materials and components that can be hazardous and cause environmental damage when disposed of improperly. This wastes is also referred to as e-waste.

Poisoned workers, environmental damage, rising materials costs, and geopolitical tensions are some of the outcomes. In addition, many electronic devices contain scarce or vaulable resources (Gold, rare earth metals, etc.). 

WEEE (Waste Electrical and Electronic Equipment Directive) addresses the complex issue of recycling waste electronics by improving the collection, treatment, and recycling of these materials at their end of life.

---
<i class="fa fa-info-circle" style="color:#4d45c2"><!-- icon --></i>

International-mindedness: 
- The export of highly toxic waste from one country to another is an issue for all stakeholders.

Theory of knowledge:
- The circular economy can be seen as an example of a paradigm shift in design. Does knowledge develop through paradigm shifts in all areas of knowledge?

Utilization:
- Design technology topics 4, 8 and 10.
- Environmental systems and societies topic 8.

Concepts and principles:
- Re-use
- Recycle
- Repair
- Recondition
- Re-engineer
- Pollution/waste
- Methodologies for waste reduction and designing out waste
- Dematerialization
- Product recovery strategies at end of life/disposal
- Circular economy—the use of waste as a resource within a closed loop system

Guidance:
- Use and recovery of standard parts at the end of product life
- Recovery of raw materials
- Reduction of total material and energy throughput of a product or service, and the limitation of its environmental impact through: reduction of raw materials at the production stage; energy and material inputs at the user stage; waste at the disposal stage.
- How dematerialization can improve product efficiency by saving, reusing or recycling materials and components
- The impacts of dematerialization on each stage of the product life cycle including: material extraction; eco-design; cleaner production; environmentally conscious consumption patterns; recycling of waste
- Potential results of successful dematerialization
---
