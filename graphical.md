# 3: Modelling
## 3.2 Graphical modelling
---
<i class="fa fa-info-circle" style="color:#4d45c2"><!-- icon --></i>

Essential idea

>Graphical models are used to communicate design ideas.

Nature of design:

>Graphical models can take many forms, but their prime function is always the same—to simplify the data and present it in such a way that understanding of what is being presented aids further development or discussion.
Designers utilise graphical modelling as a tool to explore creative solutions and reﬁne ideas from the technically impossible to the technically possible, widening the constraints of what is feasible.

Aim:

>The development of ideas through graphical models allows designers to explore and deepen their understanding of a problem and context of use.

---

After the conceptual model has been defined, the product design starts with sketching and modelling. Drawings in general are an effective and easy way to visualize and communicate ideas.

From the book (P. Metcalfe, R. Metcalfe, Design & Technology):

>Models exist to support decisions, they attempt to answer questions around ‘what if’.

Models help express ideas among a group of peers working on a project. This stage of the process allows for experimentation, evaluation, freedom and creativity.

---

## Mathematical models

These models offer opportunities to forecast behavior, make predictions or inform of possible opportunities.

Mathematical models can also provide

- scientific data for product improvement
- market forecasts
- price vs investment data
- performance scenarios
- optimal conditions to aspire to
- profits optimization data

---

## 2D and 3D graphical models

Hand drawn sketches allow for freedom and speed, and may have a huge impact on the final stages of the design. Sketches can be 2D and/or 3D, but in all probability, more complex and formal models will be required ahead in the process.

Annotations are also part of the graphical models. They further explain ideas or features to deepen the model and make it more useful for the viewer. Annotations help:

- identify problems for future resolution
- clarify obscure or difficult-to-sketch concepts
- note a range of related thematic variations or alternatives
- record their thoughts about particular features for later reference

## Perspective drawings

Perspective drawings: A set of formal drawing techniques that depicts an object as getting smaller and closer together the further away they are. The techniques are one-point perspective, two-point perspective, and three-point perspective.

The most important characteristics of perspective drawings are:

- objects appear smaller as their distance from the observer increases
- an object’s dimensions are foreshortened along the line of sight compared with those dimensions across the line of sight
- objects appear as an approximate as if perceived by the eye

![Perspective drawing examples](images/3_2_one_two_three_-point_perspective.png)

Source: https://blog.keyshot.com/2016/use-shift-lens-setting-keyshot

## Projection drawings

Projections are systems of drawings that are accurately drawn, the two main types are isometric projection (formal drawing technique) and orthographic projection (working drawing technique).

**Isometric**: a method of representing three-dimensional objects on a flat surface by means of a drawing that shows three planes of the object.

**Orthographic**: a method for representing a three-dimensional object by means of several views from various planes.

![Isometric and orthographic example](images/3_2_isometric_orthographic.png)

## Scale drawings

Drawings that are bigger or smaller than the real product, but exactly in proportion with the product.

## Assembly drawings

A diagram that shows how components fit together to make a whole. Typically presented in an exploded view.

![Perspective drawing examples](images/3_2_Serrage_flasque_corps_pompe_engrenages.png)

---
