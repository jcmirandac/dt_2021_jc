# IB design technology
###2021
##this site contains information from different sources, including IB material cited here for educational purposes.

##by juan carlos miranda castro

---
<i class="fa fa-info-circle" style="color:#4d45c2"><!-- icon --></i> This guide contains additional information on the IB design technology course.

As we advance in the course, I will add the new topics to this online guide.

---

Any IB material cited here is property of the International Baccalaureate Organization (IBO).
