# Psychological factors

---
<i class="fa fa-info-circle" style="color:#4d45c2"><!-- icon --></i>

Essential idea: 

Nature of design:

Aims:

---

Human beings vary psychologically in complex ways. Any attempt by designers to classify people into groups merely results in a statement of broad principles that may or may not be relevant to the individual. Design permeates every aspect of human experience and data pertaining to what cannot be seen such as touch, taste, and smell are often expressions of opinion rather than checkable fact.

The analysis of the human information processing system requires a designer to critically analyse a range of causes and effects to identify where a potential breakdown could occur and the effect it may have.

### Psychological factor data

Human factor data related to psychological interpretations caused by light, smell, sound, taste, temperature and texture.

* These factors can better help understand and optimise the user’s safety, health, comfort and performance.
* These are a significant part of ergonomics and human efficiency, comfort and safety can be affected by these factors.

**Methods of Collecting Psychological Factor Data**

Data Collection through four types of measurement data scales, which include; nominal, ordinal, interval and ratio.

<iframe width="560" height="315" src="https://www.youtube.com/embed/rL38g06DbSc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

**Nominal (Data) Scale** – Nominal means ‘by name’ and used in classification or division of objects into discrete groups. Each of which is identified with a name e.g. category of cars, and the scale does not provide any measurement within or between categories.

* Nominal scales are very weak, as they do not tell you anything more than that one object is different from another.
* Quantitative assessment cannot be made.

<img src="images/1_1_b_nominal.png" width=400>

---

**Ordinal (Data) Scale** – A statistical data type that exists on an arbitrary numerical scale where the exact numerical value has no significance other than to rank a set of data points. Deals with the order or position of items such as words, letters, symbols or numbers arranged in a hierarchical order. Quantitative assessment cannot be made.

* When numerals are used, they only indicate sequence or order, for example, ranking someone by placing them in a competition as “third” rather than by a score—they may have come third with 50% right or with 75%.
* Quantitative assessment cannot be made.

<img src="images/1_1_b_ordinal.png" width=400>

---

**Interval (Data) Scale** –  Interval data are based on numeric scales in which we know the order and the exact difference between the values (every pont is placed at an equal distance for the other). Organised into even divisions or intervals, and intervals are of equal size. The distance between two points is standarized.

* An interval scale is a more powerful scale, as the intervals or difference between the points or units are of an equal size (for example, in a temperature scale like Celsius, the difference between 60 degrees and 50 degrees is the same as 10 degrees and 20 degrees).
* Measurements using an interval scale can be subjected to numerical or quantitative analysis.

<img src="images/1_1_b_thermometer.jpg" width=200>

Image from https://commons.wikimedia.org/wiki/File:Mercury_Thermometer.jpg.

---

**Ratio (Data) Scale** – A ratio scale allows you to compare differences between numbers. For example, use a rating scale of 1-10 to evaluate user responses.

* The difference between a ratio scale and an interval scale is that the zero point on an interval scale is some arbitrarily agreed value, whereas on a ratio scale there is a true zero.
* For example, 0°C has been defined arbitrarily as the freezing temperature of water, whereas 0 grams is a true zero, that is, no mass.
* Measurements using a ratio scale can be subjected to numerical or quantitative analysis.
* Height and weight are two examples of ratio scales.

<img src="images/1_1_b_scale.jpg" width=300>

Image from https://commons.wikimedia.org/wiki/File:Toledo_dial_scale.jpg.

---

[More info on data scales](http://www.mymarketresearchmethods.com/types-of-data-nominal-ordinal-interval-ratio/).

---

### Qualitative versus Quantitative Data

For data analysis, we may have two types of data: qualitative and quantitative. Qualitative data is used to describe, and can be categorized according to characteristics. Quantitative data can be counted, measured and expressed in numbers.

**Qualitative Data** – Typically descriptive data used to find out in depth the way people think or feel – their perception. Useful for research at the individual or small (focus) group level.

* Nominal or Ordinal Scale (Qualitative) – taste, smell, temperature and texture.
* Qualitative data may be used in a design context relating to psychological factors, but individuals vary in their reaction to the data.
>For example, one person will find a room temperature comfortable while another person will find it uncomfortable, though the temperature is constant.

Term: **Quantitative Data** – Data that can be measured and recorded using numbers. Examples include height, shoe size, and fingernail length.

* Interval or Ratio Scale (Quantitative) – sound, temperature and light.

------------------------

### Human Information Processing Systems

**Human information processing system** is an automatic system that a person uses to interpret information and react. It is normally comprised of inputs, processes (which can be sensory, central and motor), and outputs. Humans are information processing systems, just as computers are. Designers must consider how the human mind processes and acts upon information. 

These are usually presented using flow charts.

<img src="images/1_1_b_process01.png" width=600>

We can apply thses processing systems to common tasks such as being burned by hot water

<img src="images/1_1_b_process02.png" width=600>

Just as important as understanding how the mind processes information, it understanding how this system can break down. If there is too much stimulus (Input), or if there is no way to process the information, an error, mistake or accident can occur.


<iframe width="560" height="315" src="https://www.youtube.com/embed/piDEgCefgKI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Take for example a driver of a car trying to answer a phone. The human brain can only process so much stimulus at a given time: reaching for the ringing phone, driving the car, paying attention to traffic signals, and noticing oncoming cars all place demands on the brain. There is simply too much information to processes quickly and accurately. The brain cannot cope, and something will get missed (not noticing somebody crossing the road?) and an accident will happen.

Many factors can affect how well we process information:

* Age: the user may be too young and not have learnt the skills to accomplish the task; the use may be too old and have forgotten how to do the task. e.g. The fine-motor skills of young children may have not developed enough for them to turn a dial precisely
* Strength: the user may be too weak to do the task. e.g. They may not be strong enough to press a switch or pull a lever.
* Skills: the user may not have learnt the skills yet, or the skills may be to complex to learn without a lot of practice. e.g. Riding a bicycle on a flat surface requires practice before it can be done well. Riding a mountain bike downhill at speed requires even more practice.
* Health: the user may not be physically or mentally healthy enough to carry out the task. e.g. They may be injured or have a condition which prevents them from doing the task easily.

Environmental Factors:

Users respond to environmental factors and these can affect how they perform. Environmental factors include sound, temperature, lighting, air quality, and smell. Environmental factors can have effects on Comfort, Alertness, and Perception

Designers should understand that environmental factors can affect people in different ways. A room temperature that is comfortable to one person might be too hot or too cold for another. Likewise, a person who is mobile in a given space is going to be warmer than a person who sits at a desk (Think of teachers and students). Studies also show that women typically feel colder than men do, and thus prefer a warmer temperature. 

Alertness

Alertness refers to the ability to be focused and awake. Environmental factors will affect our level of alertness in different ways:
* Temperature: Too hot and we get sleepy and less alert; Too cold and we become very alert
* Sound: Too loud and it can be distracting or even damaging to our hearing. Repetitive or high-pitched sounds can be annoying, distracting, or irritating.
* Lighting: Too dark or too light can cause strain on the eyes, causing headaches. Types of indoor lighting can also affect people. Florescent lighting is cooler and can cause eyestrain and headaches, for example
* Air quality: Dusty or stuffy air can make it difficult to breathe.
* Smell: Bad smells can be distracting and repulsive.

Perception

People will perceive environmental factors in different ways. While we may be able to measure an environmental factor using quantitative data (the room temperature, for example), the perception will vary from person to person.

* The users respond differently to different environmental factors.
* Perception has an impact on the accuracy and reliability of psychological factors data.
* Quantitative data may be used in a design context relating to psychological factors, but individuals vary in their reaction to the data.
* How warm or cold work environments can affect the performance of an individual.
* Thermal comfort describes a person’s psychological state of mind and involves a range of environmental factors: air temperature, the heat radiating from the Sun, fires and other heat sources, air velocity (still air makes people feel stuffy, moving air increases heat loss), humidity, and personal factors (clothing and metabolic rate).
* Hopefully in an office environment where a number of people work together, the thermal environment satisfies the majority of the people.
* Thermal comfort is not measured by air temperature (quantitative), but by the number of people complaining (qualitative) of thermal discomfort.
