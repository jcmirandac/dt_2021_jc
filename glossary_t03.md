# Glossary for topic 3

From:

Diploma Programme Design technology 
Glossary of terms 
By:

© International Baccalaureate Organization 2015 

Topic 3 : Modelling

| Term | Definition |
| -- | -- |
| **Aesthetic models** | A model developed to look and feel like the final product. |
| **Animation** | The ability to link graphic screens together in such a way as to simulate motion or a process. |
| **Assembly drawings** | A diagram that shows how components fit together to make a whole. Typically presented in an exploded view. |
| **Bottom-up modelling** | A designer creates part geometry independent of the assembly or any other component. Although there are often some design criteria established before modelling the part, this information is not shared between models. Once all parts are completed, they are brought together for the first time in the assembly. |
| **Computer Aided Design (CAD)** | The use of computers to aid the design process. |
| **Conceptual modelling** | A model that exists in the mind used to help us know and understand ideas. |
| **Data Modelling** | A model that determines the structure of data. |
| **Digital human** | Computer simulation of a variety of mechanical and biological aspects of the human body. |
| **Fidelity** | The degree to which a prototype is exactly like the final product. |
| **Finite element analysis (FEA)** | The calculation and simulation of unknown factors in products using CAD systems. For example, simulating the stresses within a welded car part. |
| **Formal drawing techniques** | A type of drawing technique that has fixed rules, the most widely used being isometric projection and perspective drawing. |
| **Fused deposition modelling (FDM)** | A 3D printing technique that places melted layers of material on a bed to build up a 3D model. |
| **Graphical models** | A visualization of an idea, often created on paper or through software, in two or three dimensions. |
| **Haptic technology** | Haptic technology is an emerging technology that interfaces the user via the sense of touch. |
| **Instrumented models** | Prototypes that are equipped with the ability to take measurements to provide accurate quantitative feedback for analysis. |
| **Laminated object manufacturing (LOM)** | A system that virtually slices a 3D CAD model into thin layers, then cuts out each layer from a roll of material using a laser or plotter cutter. The layers can then be glued in the correct order to create a 3D model. |
| **Mock-ups** | A scale or full-size representation of a product used to gain feedback from users. |
| **Motion capture** | The recording of human and animal movement by any means, for example, by video, magnetic or electro-mechanical devices. |
| **Part drawings** | Orthographic drawings of the components of an assembly containing details just about that component. |
| **Perspective drawings** | A set of formal drawing techniques that depicts an object as getting smaller and closer together the further away they are. The techniques are one-point perspective, two-point perspective, and three-point perspective. |
| **Physical modelling** | The creation of a smaller or larger tangible version of an object that can be physically interacted with. |
| **Projection drawings** | Systems of drawings that are accurately drawn, the two main types are isometric projection (formal drawing technique) and orthographic projection (working drawing technique). |
| **Prototypes** | A sample or model built to test a concept or process, or to act as an object to be replicated or learned from. Prototypes can be developed at a range of fidelity and for different contexts. |
| **Scale drawings** | Drawings that are bigger or smaller than the real product, but exactly in proportion with product. |
| **Scale models** | A model that is either a smaller or larger physical copy of an object. |
| **Selective laser sintering (SLS)** | An additive manufacturing technique that uses a laser to fuse small particles of material into a mass that has a desired 3D shape. |
| **Sketches** | Rough drawings of ideas used to convey or refine the idea. |
| **Solid modelling** | Solid models are clear representations of the final part. They provide a complete set of data for the product to be realized. |
| **Stereo-lithography** | A modelling technique that creates 3D models layer-by-layer by hardening molecules of a liquid polymer using a laser beam. |
| **Surface modelling** | A realistic picture of the final model, offering some machining data. Surface models contain no data about the interior of the part. |
| **Top-down modelling** | “Top down” design is a product development process obtained through 3D, parametric and associative CAD systems. The main feature of this new method is that the design originates as a concept and gradually evolves into a complete product consisting of components and sub-assemblies. |
| **Virtual prototyping** | Photorealistic CAD-based interactive models that use surface and solid modelling. They can be considered 'digital mock-ups'. |
| **Virtual reality (VR)** | The ability to simulate a real situation on the screen and interact with it in a near-natural way. |
| **Working drawings** | Drawings that are used to guide the production of a product, most commonly orthographical projection, section drawings, part drawings, assembly drawings and plan drawings. |
| -- | -- |
