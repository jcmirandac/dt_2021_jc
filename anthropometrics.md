# Human factors and ergonomics

---
<i class="fa fa-info-circle" style="color:#4d45c2"><!-- icon --></i>

Essential idea: 

Nature of design:

Aims:

---

## Anthropometrics

Anthropometry is the study of human body properties such as height, mass and volume and is used extensively in the design of consumer goods.

The World Health Organisation (WHO), views anthropometry as:
>“The singly most universally applicable, inexpensive and non-invasive method available to assess the size, proportions, and composition of the human body.”

Structural or static anthropometry includes data from measurements such as those made between joints. Data is recorded using standardised equipment such as [calipers](https://en.wikipedia.org/wiki/Calipers), [stadiometer](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwjN5erUl7L0AhXySTABHTfsBXQQFnoECAYQAQ&url=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FStadiometer&usg=AOvVaw1uk1hu46JjDj7raOK27h5U) and [anthropometer](https://www.google.com/search?q=anthropometer&tbm=isch&ved=2ahUKEwjFls-bl7L0AhVRW98KHd65CIIQ2-cCegQIABAA&oq=anthropometer&gs_lcp=CgNpbWcQAzIFCAAQgAQyBQgAEIAEMgUIABCABDIGCAAQBRAeMgYIABAFEB4yBggAEAUQHjIGCAAQBRAeMgQIABAYMgQIABAYMgQIABAYUNwMWP4QYN8SaABwAHgAgAGiAYgBwAKSAQMwLjKYAQCgAQGqAQtnd3Mtd2l6LWltZ8ABAQ&sclient=img&ei=kM6eYcWOPNG2_Qbe86KQCA&bih=815&biw=1440&client=ubuntu&hs=xj5). It is easy to collect because the subject is not moving. Anthropometric data sets include for example height, weight and data related to various body structures in a population. Data sets can vary significantly between populations.

Functional or dynamic anthropometry includes data obtained while the subject is moving. It can be related to range and reach for some body movements, or to comfort angles and/or positions while sitting at a desk, driving, walking, etc. This kind of data is more diffcult to obtain, but is often of greater use because it demonstrates the range and ease with which movements can be made. Reaction times, reach arcs, grip strength, etc. are all examples of dynamic data.

Dynamic data helps design for example: can openers, car console features, book shelving reach, etc.

<img src="images/1_1_a_dynamic_anthropometry.png" width=200>

Image taken from [this link](https://commons.wikimedia.org/wiki/File:Computer_Workstation_Variables_cleanup.png).

When designing, there is a need for measurements to be undertaken in a methodical, standardized manner that allows confidence in the data collection.

<iframe width="560" height="315" src="https://www.youtube.com/embed/HtFC2AD2oPM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Primary data versus secondary data.

Primary data is collected by the designer, who performs anthropometric measurements on the proposed user group. Because they are performed on an user or group it relates directly to the intended population.

Secondary includes a big number of anthropometric data. Data is collected into databases. Such databases are often national in nature. On [this link](https://guides.library.ualberta.ca/anthropometric-data/websites-united-states) you can consult athropometric databases.

### Percentiles and percentile ranges.

Percentiles represent 100 equal groups in which a sample can be divided. It goes from 1 to 99. For example, the 50th percentile divides the sample in two equal groups, 50% are below that value, and 50% above. The 75th percentile is the value for which 75% is lower, and 25% is higher.

A percentile range defines a spread of values. A normal distribution is that which concentrates the majority of values around the average or mean value, which is the 50th percentile. This 50th percentile represents the most likely value. When designing, the success of a product might be linked to a certain percentile correctly defined according to what is being designed and for whom.

The 5th through 95th percentile range covers 90% of people. Only 5% at the top and bottom of the data are excluded. This makes sense if we consider that the normal distribution will concentrate the majority of samples around the median, and not the extremes.

Extremes will always require special consideration.

**Mean± SD and key percentiles of anthropometric dimensions in two genders**

<img src="images/1_1_a_mean-SD-and-key-percentiles-of-anthropometric-dimensions-in-two-genders.png" width=500>

Image taken from [this link](https://www.researchgate.net/figure/Mean-SD-and-key-percentiles-of-anthropometric-dimensions-in-two-genders_tbl2_273777425).

From the table shown above:

1. What is the sitting eye height of the 50th percentile of females?
2. Which is the shortest arm lenght for males and which group does it belong to?

[More information on percentiles](http://www.ergonomics4schools.com/lzone/anthropometry.htm).

### Clearance, Reach and Adjustability

Term: Clearance: The physical space between two objects.

>eg: two people in a doorway or the space between sitting people.

Term: Reach: A range that a person can stretch to touch or grasp an object from a specified position.

>eg arm extension or work envelope.

Term: Adjustability: The ability of a product to be changed in size, commonly used to increase the range of percentiles that a product is appropriate for. 

>Car driver seats have many adjustments that can accommodate many people, eg seat height, distance to sterling wheel, even height of the steering wheel.
>Often adjustability is used in design contexts where a range of sizes is not possible or expensive to produce.

### Range of sizes versus Adjustability

Term: Range of sizes: A selection of sizes a product is made in that caters for the majority of a market.

>Clothes or motor cycle helmets come in a range of sizes to accommodate as many percentiles ranges.
>This is often done for comfort and safety. If something is not comfortable then it can lead to unsafe situations due to fatigue.

Bicycles use a combination of range of sizes (to suit different heights) as well adjustability (the seat and handle bars).

Consider how products can be adaptable for different markets or adjustable to cater for most.
