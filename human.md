# Human factors and ergonomics

---
<i class="fa fa-info-circle" style="color:#4d45c2"><!-- icon --></i>

Essential idea: 

Nature of design:

Aims:

---

##  Human factors

The International Ergonomics Association defines ergonomics or human factors as follows:

>Ergonomics (or human factors) is the scientific discipline concerned with the understanding of interactions among humans and other elements of a system, and the profession that applies theory, principles, data and methods to design to optimize human well-being and overall system performance.

In some cases, Human factors refers to the combination of **ergonomics** and **anthropometrics**. It is a science focused on understanding what affects our performance and behaviour in order to understand how we function mentally and physically, and therefore, help us design elements of our environment.

Human factors can help us make it easier to do the right thing (harder to do the wrong thing). It describes how we interact with "everything". It applies psychological and physiological principles to the engineering and design of products, processes, and systems. The goal of human factors is to reduce human error, improve performance, enhance security and comfort, all of this in relation to the interaction between the human and a thing of interest.

<iframe width="560" height="315" src="https://www.youtube.com/embed/m4f81ZS19v8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
