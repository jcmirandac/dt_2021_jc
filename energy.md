# 2: Resource management and sustainable production
## 2.3 Energy utilization, storage and distribution
---
<i class="fa fa-info-circle" style="color:#4d45c2"><!-- icon --></i>

Essential idea

>There are several factors to be considered with respect to energy and design.

Nature of design:

>Efficient energy use is an important consideration for designers in today’s society. Energy conservation and efficient energy use are pivotal in our impact on the environment. A designer’s goal is to reduce the amount of energy required to provide products or services using newer technologies or creative implementation of systems to reduce usage. For example, driving less is an example of energy conservation, while driving the same amount but with a higher mileage car is energy efficient. (1.11, 1.16, 2.10)

Aim 1:

>As we develop new electronic products, electrical energy power sources remain an ever-important issue. The ability to concentrate electrical energy into ever-decreasing volume and weight is the challenge for designers of electronic products.

---

Energy efficiency and energy conservation are two extremely important concepts in today's world, especially the design and production fields.

Environmentally friendly products are sought after by customers aware of the current environmental deterioration of the planet.

From a designer's point of view, production must be done more efficiently, with processes and materials that require less energy to produce, distribute, sell and dispose of.

###Embodied energy:

From the book (P. Metcalfe, R. Metcalfe, Design & Technology):

>Embodied energy represents an assessment of all of the energy associated with a product throughout its life cycle, including the energy required to obtain, process and transport the raw materials used in its manufacture. The embodied energy throughout the product life cycle ‘cradle to grave’ is therefore an energy balance involving the embodied energy of manufacture plus the embodied energy of operations/ maintenance/repair plus the embodied energy of dismantling/retrieval/disposal.

Term: **Embodied energy** – The total energy required to produce a product.

Term: **Energy utilization** – The method with which energy is used.

* Total energy consumed in production (cradle to [factory] gate) and throughout the lifecycle of a product (cradle to grave)
* Is the sum of all energy needed to produce a product or service.
* It is highly useful to calculate how successful/effective a product or service produces or saves energy.

<iframe width="560" height="315" src="https://www.youtube.com/embed/sQlyvSzochA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/1CxVP0mgLBA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/6rgn5V0Kon0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Using the concept of embodied energy, the environmentally friendly credentials of a wide variety of disparate products can be tested. 

Measurements of embodied energy can be expressed in a number of ways such as the megajoules of energy expended per kilogram of product (Energy MJ/kg), or as embodied carbon expressed as kg (or tonnes) of CO₂  released per kilogram of product (kgCO₂/kg). Estimates of embodied energy for various materials can differ signiffcantly, indicating the diffculty in making such assessments.

Consult the book for embodied energy for a small list of materials on page 26.

https://www.theverge.com/22628925/water-semiconductor-shortage-arizona-drought

https://energyeducation.ca/encyclopedia/Embodied_energy

---

### Distributing energy: national and international grid systems

Thomas Edison created in 1882 the first power plant in New York, supplying DC.

![Edison power plant](images/2_2_Edision_PowerPlant,_Pearl_Street_NYC.jpg)

Nikola Tesla and George Westinghouse built the first AC power plant at Niagara Falls.

![Tesla-Westinghouse power plant](images/2_2_AdamsPowerPlant.jpg)

For a number of years, there was a battle between Edison and Tesla over the most suitable style of current for public distribution.

AC cannot be stored in a battery for later use, but it can be transformed to higher or lower voltages. This is an advantage because by stepping up AC voltage, electricity can be trasnmitted over long distances. Eventually, high voltages are stepped down by local transformers for domestic use.

DC voltage cannot easily be changed to reduce transmission losses through a conductor. This means that DC cannot usually be sent efficiently over long distances.

In Costa Rica, close to 99% of the population has access to locally distributed electricity. Costa Rica has a strong distribution system which includes hydroelectrical, wind, geothermal, biomass and solar energy production.

![Cachi hydroelectric power plant](images/2_2_represa_cahi.png)

>Cachi hydroelectric power plant.

Term: **National and international grid systems** – An electrical supply distribution network that can be national or international. International grids allow electricity generated in one country to be used in another.

Term: **Energy Distribution** -The method with which energy is transported from a source to where it is used.

The supply of electricity supported the rapid growth of infrastructure and economic development of industrialized nations. In many situations the expansion of government built generation plants was boosted by the needs of WWI.

Energy is distributed over a national and international grid systems. Today, most modern communities receive their electricity supplies through connection to a regional or national grid. These grids comprise a number of electricity production centres strategically positioned around the country. Elecgtric grids are fundamental to industry and economic development in a country or region.

![Red de distribución eléctrica Costa Rica](images/2_2_electricity_grid_simple.png)

>Image in public domain.

![Red de distribución eléctrica Costa Rica](images/2_2_red_costa_rica.png)

Taken from [this document](http://www.acesolar.org/wp-content/uploads/2017/06/ESTUDIO-BID-DE-RED-CR-AN%C3%83-LISIS-DE-OPCIONES-PARA-REV-ABRIL-2017.compressed.pdf)

![Red de distribución eléctrica Centroamérica](images/2_2_SIEPAC_red.png)

Taken from [this document](http://www.acesolar.org/wp-content/uploads/2017/06/ESTUDIO-BID-DE-RED-CR-AN%C3%83-LISIS-DE-OPCIONES-PARA-REV-ABRIL-2017.compressed.pdf)

With the exception of hydro-electricity, the production of base-load electrical power is achieved using steam driven turbines. Heat used to turn water into steam is supplied by nuclear energy or the combustion of fossil fuels such as coal or gas.

<iframe width="560" height="315" src="https://www.youtube.com/embed/4L31dHXP6i0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

### Local Combined Heat and Power (CHP)

Term: A system that simultaneously generates heat and electricity from either the combustion of fuel, or a solar heat collector.

Local combined heat and power is a technology that uses a single fuel source to produce both heat and electricity. This type of system means a consumer does not have to purchase energy from a local utility in addition to burning a fuel on-site to generate heat.

CHP systems have the advantages of reduced costs because heating and energy production are combined into one system and reduced emissions because of the combined system.

In CHP, the waste heat produced in a plant facility is used for other industrial processes, or distributed to buildings or to a district heating system.

Advantages of CHP:

* It is an efficient and clean approach to generating electric power and thermal energy from a single fuel source.
* It can either replace or supplement conventional separate heat and power.
* Instead of purchasing electricity from the local utility company and burning fuel (oil, gas etc) in an on-site furnace or boiler to produce thermal energy.
* Reduces the negative impact to the environment.
* Saves the consumer money.

<iframe width="560" height="315" src="https://www.youtube.com/embed/eDjZhOxZHME" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/AdNkKVAF_zg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<img src="images/2_2_Combined-Heat-Power-CHP-system.png" width=500>

Image taken from [this link](https://www.researchgate.net/figure/Combined-Heat-Power-CHP-system_fig4_278389893)

---

### Systems for Individual Energy Generation

Term: **Individual energy generation** is the ability of an individual to use devices to create small amounts of energy to run low-energy products.


* is the small-scale generation of heat and electric power by homes (also small businesses and small communities) to meet their own needs.
* It is an alternative or can supplement traditional centralized grid-connected power.
* Lower negative impact on the environment
* Lower costs for the consumer
* High initial capital cost
* Can sell excess electrical power back to the National Grid
* Also known as micro-generation

<iframe width="560" height="315" src="https://www.youtube.com/embed/Ci_yPXmi5YY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---
<i class="fa fa-info-circle" style="color:#4d45c2"><!-- icon --></i>

International-mindedness: 
- There are instances of energy sources (for example, oil and electricity) crossing national boundaries through cross-border networks leading to issues of energy security.

Theory of knowledge:
- The Sun is the source of all energy and essential for human existence. Is there some knowledge common to all areas of knowledge and ways of knowing?

Utilization:
- Design technology topics 8 and 10
- Biology topic 4
- Chemistry option C
- Environmental systems and societies topic 2
- Physics topics 5, 8 and 11

Concepts and principles:
- Embodied energy
- Distributing energy: national and international grid systems
- Local combined heat and power (CHP)
- Systems for individual energy generation
- Quantification and mitigation of carbon emissions
- Batteries, capacitors and capacities considering relative cost, efficiency, environmental impact and reliability

Guidance:
- Total energy consumed in production (cradle to [factory] gate) and throughout the lifecycle of a product (cradle to grave)
- Batteries are limited to hydrogen fuel cells, lithium, NiCad, lead acid, and LiPo batteries
---
