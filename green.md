# 2: Resource management and sustainable production
## 2.5 Green design
---
<i class="fa fa-info-circle" style="color:#4d45c2"><!-- icon --></i>

Essential idea

>Green design integrates environmental considerations into the design of a product without compromising its integrity.

Nature of design:

>The starting point for many green products is to improve an existing product by redesigning aspects of it to address environmental objectives. The iterative development of these products can be incremental or radical depending on how eﬀectively new technologies can address the environmental objectives. When newer technologies are developed, the product can re-enter the development phase for further improvement.

Aim:

>The purpose of green design is to ensure a sustainable future for all.

---

From the book (P. Metcalfe, R. Metcalfe, Design & Technology):

>Designers and manufacturers are increasingly considering the relationship between their products and the environment. Products on shelves everywhere proclaim their environmental credentials using terms such as ‘environmentally friendly’, ‘sustainable’, ‘eco friendly’ and ‘green’. Quotes of international standards and certiﬁcation abound.

Green design refers to products or services which impact on the environment through its life cycle has been purposely reduced. The design process of these products/services was developed on the premise of low impact to the environment. This includes al stages: material extraction, manufacturing, use, maintenance and end of life disposal.

##Incremental and radical strategies

Incrememntal strategies:

* process streamlining (simplify or eliminate unnecessary work-related tasks)

* product optimisation

* parts standardisation

* improvements in reliability

* benchmarking against competitors (the practice of comparing business processes and performance metrics to industry bests and best practices from other companies)

Radical strategies:

These are more prominent changes in the designing process that may result in a new product or service altogether. They can push forward the market and technology, but involve greater risk and investment.

##Green legislation

Eco–labelling is a legislated requirement within the EU. This program began in 1992 to certify that products met strict measures regarding production and disposal of manufactured goods.

Environmental certifications on products can be cosidered as a competitive advantage. Adherence to these environmental management standards may make companies eligible for certiﬁcation. This may then in turn be a requirement of their suppliers and thus certiﬁcation could become a prerequisite for doing business in some ﬁelds.

##Design objectives for green products

The World Business Council for Sustainable Development has developed a list of criteria intended to reﬂect the principles or objectives associated with the development of green products and includes the following:

* Reducing toxic dispersion.

* Extending product durability.

* Enhancing material durability.

* Maximizing sustainable use of renewable resources.

* Increasing the serviceability of goods and services.

* Reducing the material and energy intensity of goods and services.

* Potentially requiring certiﬁcation to become a prerequisite for doing business in some ﬁelds.

##Strategies for designing green products

* longevity

* disassembly

* reduced waste

* energy efficiency

* dematerialisation

* systems integration

* recyclability and repair

* reduced embodied energy.

<iframe width="560" height="315" src="https://www.youtube.com/embed/aCBAWPHOnLw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/EhtA_sXY-wY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/Gt-EpA8vwgk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

<i class="fa fa-info-circle" style="color:#4d45c2"><!-- icon --></i>

International-mindedness: 
- The ability and will of diﬀerent countries to enact environmental legislation varies greatly.

Theory of knowledge:
- Green issues are an area where experts sometimes disagree. On what basis might we decide between the judgements of experts if they disagree?

---
