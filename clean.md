# 2: Resource management and sustainable production
## 2.4 Clean technology
---
<i class="fa fa-info-circle" style="color:#4d45c2"><!-- icon --></i>

Essential idea

>Clean technology seeks to reduce waste/pollution from production processes through radical or incremental development of a production system.

Nature of design:

>Clean technology is found in a broad range of industries, including water, energy, manufacturing, advanced materials and transportation. As our Earth’s resources are slowly depleted, demand for energy worldwide should be on every designer’s mind when generating products, systems and services. The convergence of environmental, technological, economic and social factors will produce more energy eﬃcient technologies that will be less reliant on obsolete, polluting technologies.

Aim:

>The legislation for reducing pollution often focuses on the output and therefore, end of pipe technologies. By implementing ideas from the Circular Economy, pollution is negated and waste eliminated.

---

Energy efficiency and energy conservation are two extremely important concepts in today's world, especially the design and production fields.

Environmentally friendly products are sought after by customers aware of the current environmental deterioration of the planet.

From a designer's point of view, production must be done more efficiently, with processes and materials that require less energy to produce, distribute, sell and dispose of.

##Drivers for cleaning up manufacturing:

Drivers can be classified into three groups: **social**, **political** and **economical**.

People expect operate under the impression that manufacturing plants work in such a way that any harm to the environment is reduced, neutralized or countered. It is also expected that employees will not be exposed to harmful situations. Protests and negative publicity are two actions through which communities can influence the adoption of clean technologies.

Political decisions and environmental legislation have a direct impact on manufacturing processes. Acceptable pollution levels for water, air, etc., set the goals for production entities. 

Legislation and independent monitoring ar the tools employed by governments con ensure compliance and sustainability.

Targets of continued improvement are important to reduce the effects of manufaturing and production processes over the environment. They also lead to reserach and exploration of new cleaner technologies.

New technologies can also contribute to increasing competitiveness and efficiency, however they can also be an expensive practice for manufacturers. The introduction of new technologies can be incremental or radical, depending on the requirements imposed by legislation.

##International targets for reducing pollution and waste

International legislation has often caused controversy. Equal restrictions for every country has been deemed unfair, given the fact that more developed countries have had a much bigger effect on some environmental problems than poorer, less developed countries. To this effect, Kyoto Protocol (2005) is based on the principle of "Common but different responsibilities". Meaning that all countries must agree to reduce greenhouse gas emissions, but they acknowledge that developed nations are responsible for more of those emissions, therefore, should have more strict restrictions.

##End-of-pipe technologies

It is an approach to pollution reduction focused on technology at the end of the manufacturing process to reduce the ammount of pollutants. 

In contrast, a more realistic and effective approach is to minimize the waste stream and pollutants before they are generated. This means preventing environmental damage by changing the source rather than dealing with the negative effects of pollutants and their effects.

![End of pipe technology](images/2_4_end_of_pipe.png)

<iframe width="560" height="315" src="https://www.youtube.com/embed/HwVxFDj_Q7E" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/DFhuNKNDrLg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

<i class="fa fa-info-circle" style="color:#4d45c2"><!-- icon --></i>

International-mindedness: 
- The development of clean technology strategies for reducing pollution and waste can positively impact local, national and global environments.

Theory of knowledge:
- International targets may be seen to impose the view of a certain culture onto another. Can one group of people know what is best for others?

---
