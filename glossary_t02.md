# Glossary for topic 2

From:

Diploma Programme Design technology 
Glossary of terms 
By:

© International Baccalaureate Organization 2015 

Topic 2 : Resource management and sustainable production

| Term | Definition |
| -- | -- |
| **Circular economy** | An economy model in which resources remain in use for as long as possible, from which maximum value is extracted while in use, and the products and materials are recovered and regenerated at the end of the product life cycle. |
| **Clean technology** | Products, services or processes that reduce waste and require the minimum amount of non-renewable resources. |
| **Combined Heat and Power (CHP)** | A system that simultaneously generates heat and electricity from either the combustion of fuel, or a solar heat collector. |
| **Converging technologies** | The synergistic merging of nanotechnology, biotechnology, information and communication technologies and cognitive science. |
| **Cradle to cradle** | A design philosophy that aims to eliminate waste from the production, use and disposal of a product. It centres on products which are made to be made again. |
| **Cradle to grave** | A design philosophy that considers the environmental effects of a product all of the way from manufacture to disposal. |
| **Dematerialization** | The reduction of total material and energy throughput of any product and service.  |
| **Design for the environment software** | Software that allows designers to perform Life cycle analysis (LCA) on a product and assess its environmental impact. |
| **Eco-design** | A design strategy that focusses on three broad environmental categories - materials, energy, and pollution/waste. |
| **Embodied energy** | The total energy required to produce a product. |
| **End-of-pipe technologies** | Technology that is used to reduce pollutants and waste at the end of a process. |
| **Energy distribution** | The method with which energy is transported from a source to where it is used. |
| **Energy storage** | The method with which energy is stored for later use. |
| **Energy utilization** | The method with which energy is used. |
| **Green design** | Designing in a way that takes account of the environmental impact of the product throughout its life. |
| **Green legislation** | Laws and regulations that are based on conservation and sustainability principles, followed by designers and manufacturers when creating green products. |
| **Incremental solutions** | Products which are improved and developed over time leading to new versions and generations. |
| **Individual energy generation** | The ability of an individual to use devices to create small amounts of energy to run low-energy products. |
| **Legislation** | Laws considered collectively to address a certain topic. |
| **Life cycle analysis (LCA)** | The assessment of the effect a product has on the environment through five stages of its life: pre-production; production; distribution (including packaging; utilization; and disposal. |
| **Linear economy** | An economy based on the make, use, dispose model. |
| **Local combined heat and power (CHP)** | CHP plants that generate heat and power for a local community - the plant is close enough to the community so that the heat generated can be dispersed through the community efficiently. |
| **National and international grid systems** | An electrical supply distribution network that can be national or international. International grids allow electricity generated in one country to be used in another. |
| **Non-renewable resources** | A natural resource that cannot be re-made or re-grown as it does not naturally re-form at a rate that makes its use sustainable, for example, coal, petroleum and natural gas. |
| **Product cycle** | Also known as the product life cycle, it is a cycle that every product goes through from introduction to withdrawal or discontinuation. |
| **Product recovery strategies** | The processes of separating the component parts of a product to recover the parts and materials. |
| **Quantification of carbon emissions** | Defining numerically the carbon emissions generated by a particular product. |
| **Radical solutions** | Where a completely new product is devised by going back to the roots of a problem and thinking about a solution in a different way. |
| **Recondition** | Rebuilding a product so that it is in an “as new” condition, and is generally used in the context of car engines and tyres. |
| **Recovery of raw materials** | Strategies for the separation of components of a product in order to recover raw materials. |
| **Recycle** | Recycling refers to using the materials from obsolete products to create other products. |
| **Re-engineer** | To redesign components or products to improve their characteristics or performance. |
| **Renewability** | The level at which a resource is renewable. The rate that a resource can be replenished. |
| **Renewable resources** | A natural resource that can replenished with the passage of time, or does not abate at all. |
| **Repair** | The reconstruction or renewal of any part of an existing structure or device. |
| **Reserves** | Reserves are natural resources that have been identified in terms of quantity and quality. |
| **Resources** | Resources are the stock or supply of materials that are available in a given context. |
| **Re-use** | Reuse of a product in the same context or in a different context. |
| **System level solutions** | Solutions that are implemented to deal with the whole system, rather than just components. |
| **The precautionary principle** | The anticipation of potential problems in relation to the environmental impact of the production, use and disposal of a product. |
| **The prevention principle** | The avoidance or minimization of producing waste in relation to the production, use and disposal of a product. |
| **Waste mitigation strategies** | Strategies used to reduce the waste produced by a product or in the production and disposal of a product. |
| -- | -- |
